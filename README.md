# deathIsland

Run app with one of the arguments: 
ASYNC_BY_MOVES,
SYNC_BY_MOVES,
ASYNC_BY_DURATION,
SYNC_BY_DURATION

In the resources folder:
You can change map size, amount of moves or simulation duration via simulation.properties. Frequency of operations move, reproduce, feed, barry and print statistics  can be also changed via simulation.properties for modes:ASYNC_BY_DURATION,
SYNC_BY_DURATION. 

