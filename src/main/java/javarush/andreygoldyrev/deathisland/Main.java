package javarush.andreygoldyrev.deathisland;

import javarush.andreygoldyrev.deathisland.nature.model.Animal;
import javarush.andreygoldyrev.deathisland.simulation.strategies.ESimulations;
import javarush.andreygoldyrev.deathisland.simulation.SimulationController;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        SimulationController.startSimulation(ESimulations.valueOf(args[0]));
    }
}