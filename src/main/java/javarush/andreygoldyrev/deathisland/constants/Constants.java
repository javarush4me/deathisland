package javarush.andreygoldyrev.deathisland.constants;


public final class Constants {
    public static final String NATURE_RESOURCES_PATH = "src\\main\\resources\\nature\\";
    public static final String SETTLING_THE_ISLAND = "Settling the island";
    public static final String STARTING_TO_MOVE = "Starting to move";
    public static final String STARTING_TO_REPRODUCE = "Starting to reproduce";
    public static final String STARTING_TO_FEED = "Starting to feed";
    public static final String STARTING_TO_BARY = "Starting to bary";
    public static final String OPERATIONS_FINISHED = "Operations finished = ";
    public static final String MOVE_NUMBER = "MOVE NUMBER ";
}


