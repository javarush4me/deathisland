package javarush.andreygoldyrev.deathisland.nature.model;

import lombok.ToString;


@ToString
public enum ENatureSpecies {
    WOLF(ENatureTypes.ANIMAL),
    RABBIT(ENatureTypes.ANIMAL),
    GRASS(ENatureTypes.PLANT),
    SNAKE(ENatureTypes.ANIMAL),
    FOX(ENatureTypes.ANIMAL),
    BEAR(ENatureTypes.ANIMAL),
    EAGLE(ENatureTypes.ANIMAL),
    HORSE(ENatureTypes.ANIMAL),
    DEAR(ENatureTypes.ANIMAL),
    MOUSE(ENatureTypes.ANIMAL),
    GOAT(ENatureTypes.ANIMAL),
    SHEEP(ENatureTypes.ANIMAL),
    BOAR(ENatureTypes.ANIMAL),
    BULL(ENatureTypes.ANIMAL),
    DUCK(ENatureTypes.ANIMAL),
    TRACK(ENatureTypes.ANIMAL);

    public ENatureTypes getType() {
        return type;
    }

    private final ENatureTypes type;

    ENatureSpecies(ENatureTypes type) {
        this.type = type;
    }
}

