package javarush.andreygoldyrev.deathisland.nature.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Objects;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Plant extends Nature implements Cloneable {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Plant plant = (Plant) o;
        return Objects.equals(super.getId(), plant.getId());
    }

    @Override
    public Plant clone() {
        try {
            Plant clone = (Plant) super.clone();
            clone.setId(UUID.randomUUID());
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
