package javarush.andreygoldyrev.deathisland.nature.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import java.util.Objects;
import java.util.UUID;


@Data
@EqualsAndHashCode(of = "id")
public abstract class Nature {
    private UUID id = UUID.randomUUID();
    private Double weight;
    private int maxQuantity;
    private boolean isDead;
    private ENatureSpecies species;
    private ENatureTypes type;
}
