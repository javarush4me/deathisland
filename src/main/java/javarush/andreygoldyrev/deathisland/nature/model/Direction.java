package javarush.andreygoldyrev.deathisland.nature.model;

public enum Direction {
    EAST, WEST, NORTH, SOUTH
}
