package javarush.andreygoldyrev.deathisland.nature.model;

import lombok.Getter;

@Getter
public enum ENatureTypes {
    ANIMAL(Animal.class),
    PLANT(Plant.class);
    private final Class<? extends Nature> clazz;

    ENatureTypes(Class<? extends Nature> clazz) {
        this.clazz = clazz;
    }
}
