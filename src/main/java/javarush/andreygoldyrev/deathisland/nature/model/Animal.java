package javarush.andreygoldyrev.deathisland.nature.model;

import lombok.*;


import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Data ()
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Animal extends Nature implements Cloneable {
    private Integer speed;
    private Double foodNeeded;
    private Map<ENatureSpecies, Integer> chanceToCatchPrey;

    @Override
    public Animal clone() {
        try {
            Animal clone = (Animal) super.clone();
            clone.setId(UUID.randomUUID());
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
