package javarush.andreygoldyrev.deathisland.nature.behavior.lifecycle;

import javarush.andreygoldyrev.deathisland.nature.behavior.ReproduceService;
import javarush.andreygoldyrev.deathisland.simulation.IslandSimulator;
import javarush.andreygoldyrev.deathisland.simulation.LocationSimulator;
import javarush.andreygoldyrev.deathisland.nature.model.ENatureSpecies;
import javarush.andreygoldyrev.deathisland.nature.model.Nature;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class IslandOrgy implements ILifeCycle {
    private final ReproduceService reproduceService = new ReproduceService();

    @Override
    public void launch(IslandSimulator island) {
        island.getLocations().entrySet().parallelStream()
                .forEach(locationEntry -> {
                    LocationSimulator currentLocation = locationEntry.getValue();
                    for (ENatureSpecies species : currentLocation.getFloraAndFauna().keySet()) {
                        reproduceNatureInLocation(species, currentLocation);
                    }
                });
    }

    private void reproduceNatureInLocation(ENatureSpecies species, LocationSimulator currentLocation) {
        Set<Nature> natureUnitsOfThisSpeciesInLocation = currentLocation.getFloraAndFauna().get(species);
        Iterator<Nature> iterator = natureUnitsOfThisSpeciesInLocation.iterator();
        if (iterator.hasNext()) {
            Nature nature = iterator.next();
            reproduceService.reproduce(natureUnitsOfThisSpeciesInLocation, nature);
        }
    }

    @Override
    public boolean applicable(ELifeCycleStages stage) {
        return stage.equals(ELifeCycleStages.REPRODUCE);
    }
}
