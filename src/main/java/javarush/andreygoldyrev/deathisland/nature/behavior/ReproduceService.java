package javarush.andreygoldyrev.deathisland.nature.behavior;

import javarush.andreygoldyrev.deathisland.nature.behavior.reproducing.IReproducingStrategy;
import javarush.andreygoldyrev.deathisland.nature.behavior.reproducing.ReproduceAnimals;
import javarush.andreygoldyrev.deathisland.nature.behavior.reproducing.ReproducePlants;
import javarush.andreygoldyrev.deathisland.nature.model.Nature;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class ReproduceService {
    private final List<IReproducingStrategy> BREEDING_STRATEGIES = Arrays.asList(new ReproduceAnimals(), new ReproducePlants());

    public void reproduce(Set<Nature> natureUnitOfThisSpeciesInLocation, Nature nature) {
        Optional<IReproducingStrategy> breedingStrategy = BREEDING_STRATEGIES.stream().filter(strategy -> strategy.applicable(nature)).findFirst();
        breedingStrategy.orElseThrow().reproduce(natureUnitOfThisSpeciesInLocation, nature);
    }
}
