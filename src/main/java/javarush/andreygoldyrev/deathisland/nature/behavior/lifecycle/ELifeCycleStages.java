package javarush.andreygoldyrev.deathisland.nature.behavior.lifecycle;

public enum ELifeCycleStages {
    SETTLE,
    MOVE,
    REPRODUCE,
    FEED,
    BARY
}
