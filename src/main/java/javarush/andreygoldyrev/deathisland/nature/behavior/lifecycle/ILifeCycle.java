package javarush.andreygoldyrev.deathisland.nature.behavior.lifecycle;

import javarush.andreygoldyrev.deathisland.simulation.IslandSimulator;
import javarush.andreygoldyrev.deathisland.simulation.LocationSimulator;

import java.util.Map;

public interface ILifeCycle {
    void launch(IslandSimulator island);

    boolean applicable(ELifeCycleStages stage);
}
