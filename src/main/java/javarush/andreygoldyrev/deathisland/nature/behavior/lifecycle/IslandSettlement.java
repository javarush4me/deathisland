package javarush.andreygoldyrev.deathisland.nature.behavior.lifecycle;

import javarush.andreygoldyrev.deathisland.simulation.IslandSimulator;
import javarush.andreygoldyrev.deathisland.simulation.LocationSimulator;
import javarush.andreygoldyrev.deathisland.utils.SimulationProperties;

import java.util.Map;
import java.util.stream.IntStream;


public class IslandSettlement implements ILifeCycle {
    private SimulationProperties properties = SimulationProperties.getInstance();

    @Override
    public void launch(IslandSimulator island) {
        IntStream.range(1, properties.getWidth() * properties.getHeight() + 1)
                .parallel()
                .forEach(index -> {
                    LocationSimulator locationSimulator = new LocationSimulator();
                    locationSimulator.settleLocation();
                    island.getLocations().put(index, locationSimulator);
                });
    }

    @Override
    public boolean applicable(ELifeCycleStages stage) {
        return stage.equals(ELifeCycleStages.SETTLE);
    }
}
