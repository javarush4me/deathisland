package javarush.andreygoldyrev.deathisland.nature.behavior.moving.utils;

import javarush.andreygoldyrev.deathisland.nature.model.Direction;

public interface IMovingCalculator {
    int calculateNumberOfLocationToMoveTo(int currentLocationNumber);

    boolean applicable(Direction direction);
}
