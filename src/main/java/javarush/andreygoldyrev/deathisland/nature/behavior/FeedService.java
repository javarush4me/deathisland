package javarush.andreygoldyrev.deathisland.nature.behavior;

import javarush.andreygoldyrev.deathisland.simulation.LocationSimulator;
import javarush.andreygoldyrev.deathisland.nature.model.Animal;
import javarush.andreygoldyrev.deathisland.nature.model.ENatureSpecies;
import javarush.andreygoldyrev.deathisland.nature.model.Nature;
import javarush.andreygoldyrev.deathisland.utils.Randomizer;

import java.util.Set;


public class FeedService {

    public void feed(LocationSimulator location, Animal animal) {
        Double satiety = 0D;
        for (ENatureSpecies species : location.getFloraAndFauna().keySet()) {
            if (species.equals(animal.getSpecies())) {
                continue;
            }
            Integer chanceToCatchPrey = animal.getChanceToCatchPrey().get(species);
            if (chanceToCatchPrey != null) {
                satiety = catchSpeciesInLocation(location, animal, species, satiety, chanceToCatchPrey);
                if (satiety == null) return;
            }
        }
        if (satiety < animal.getFoodNeeded()) {
            animal.setDead(true);
        }
    }

    private Double catchSpeciesInLocation(LocationSimulator location, Animal animal, ENatureSpecies species, Double satiety, Integer chanceToCatchPrey) {
        Set<Nature> natureOfThisSpeciesInLocation = location.getFloraAndFauna().get(species);
        for (Nature prey : natureOfThisSpeciesInLocation) {
            if (satiety >= animal.getFoodNeeded()) {
                return null;
            }
            boolean gotLuck = chanceToCatchPrey > Randomizer.chanceToCatchPrey();
            if (!prey.isDead() && gotLuck) {
                if (prey.getWeight() > animal.getFoodNeeded()) {
                    prey.setWeight(prey.getWeight() - animal.getFoodNeeded());
                    satiety += animal.getFoodNeeded();
                } else {
                    prey.setDead(true);
                    satiety += prey.getWeight();
                }
            }
        }
        return satiety;
    }
}

