package javarush.andreygoldyrev.deathisland.nature.behavior.moving.utils;

import javarush.andreygoldyrev.deathisland.nature.model.Direction;
import javarush.andreygoldyrev.deathisland.utils.SimulationProperties;


public class WestCalculator implements IMovingCalculator {
    private SimulationProperties simulationProperties = SimulationProperties.getInstance();

    @Override
    public int calculateNumberOfLocationToMoveTo(int currentLocationNumber) {
        int tail = (currentLocationNumber) % simulationProperties.getWidth();
        if (tail > 1) return currentLocationNumber - 1;
        return 0;
    }

    @Override
    public boolean applicable(Direction direction) {
        return direction.equals(Direction.WEST);
    }
}
