package javarush.andreygoldyrev.deathisland.nature.behavior.reproducing;

import javarush.andreygoldyrev.deathisland.nature.model.Nature;

import java.util.Set;

public interface IReproducingStrategy {
    void reproduce(Set<Nature> natureUnitOfThisTypeInLocation, Nature nature);

    boolean applicable(Nature nature);
}
