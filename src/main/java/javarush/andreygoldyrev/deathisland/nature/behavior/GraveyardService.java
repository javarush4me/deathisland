package javarush.andreygoldyrev.deathisland.nature.behavior;

import javarush.andreygoldyrev.deathisland.simulation.LocationSimulator;
import javarush.andreygoldyrev.deathisland.nature.model.ENatureSpecies;
import javarush.andreygoldyrev.deathisland.nature.model.Nature;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class GraveyardService {
    private Map<ENatureSpecies, Set<Nature>> GRAVEYARD = new ConcurrentHashMap<>();

    public void clearIsland(Map<Integer, LocationSimulator> locations) {
        locations.entrySet().parallelStream()
                .forEach(entry -> {
                    LocationSimulator currentLocation = entry.getValue();
                    for (ENatureSpecies species : currentLocation.getFloraAndFauna().keySet()) {
                        removeBodiesFromLocation(species, currentLocation);
                    }
                });
    }

    private void removeBodiesFromLocation(ENatureSpecies species, LocationSimulator currentLocation) {
        Set<Nature> natureOfThisSpeciesInLocation = currentLocation.getFloraAndFauna().get(species);
        ArrayList<Nature> listCopy = new ArrayList<>(natureOfThisSpeciesInLocation);
        for (Nature nature : listCopy) {
            if (nature.isDead()) {
                if (GRAVEYARD.get(nature.getSpecies()) == null) {
                    Set<Nature> deadNature = ConcurrentHashMap.newKeySet();
                    deadNature.add(nature);
                    GRAVEYARD.put(nature.getSpecies(), deadNature);
                } else {
                    GRAVEYARD.get(nature.getSpecies()).add(nature);
                }
                natureOfThisSpeciesInLocation.remove(nature);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (ENatureSpecies species : GRAVEYARD.keySet()) {
            stringBuilder.append(species.name()).append(": ");
            Set<Nature> natureList = GRAVEYARD.get(species);
            stringBuilder.append(natureList.size()).append("\r\n");
        }
        return "GRAVEYARD: " + "\r\n" + stringBuilder;
    }
}
