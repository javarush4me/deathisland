package javarush.andreygoldyrev.deathisland.nature.behavior.reproducing;

import javarush.andreygoldyrev.deathisland.nature.model.ENatureTypes;
import javarush.andreygoldyrev.deathisland.nature.model.Nature;
import javarush.andreygoldyrev.deathisland.nature.model.Plant;

import java.util.Set;


public class ReproducePlants implements IReproducingStrategy {
    @Override
    public void reproduce(Set<Nature> natureUnitOfThisTypeInLocation, Nature nature) {
        Plant plantToReproduce = (Plant) nature;
        int offspringCount = 0;
        for (Nature plant : natureUnitOfThisTypeInLocation) {
            if (!plant.isDead()) {
                offspringCount++;
            }
        }
        for (int i = 1; i <= offspringCount * 2; i++) {
            if (natureUnitOfThisTypeInLocation.size() < nature.getMaxQuantity()) {
                natureUnitOfThisTypeInLocation.add(plantToReproduce.clone());
            }
        }
    }

    @Override
    public boolean applicable(Nature nature) {
        return nature.getType().equals(ENatureTypes.PLANT);
    }
}
