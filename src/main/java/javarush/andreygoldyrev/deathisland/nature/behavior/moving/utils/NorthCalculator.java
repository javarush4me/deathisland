package javarush.andreygoldyrev.deathisland.nature.behavior.moving.utils;

import javarush.andreygoldyrev.deathisland.nature.model.Direction;
import javarush.andreygoldyrev.deathisland.utils.SimulationProperties;


public class NorthCalculator implements IMovingCalculator {
    private SimulationProperties simulationProperties = SimulationProperties.getInstance();

    @Override
    public int calculateNumberOfLocationToMoveTo(int currentLocationNumber) {
        int locationNumberToMoveTo = currentLocationNumber - simulationProperties.getWidth();
        if (locationNumberToMoveTo < 1) {
            return 0;
        }
        return locationNumberToMoveTo;
    }

    @Override
    public boolean applicable(Direction direction) {
        return direction.equals(Direction.NORTH);
    }
}
