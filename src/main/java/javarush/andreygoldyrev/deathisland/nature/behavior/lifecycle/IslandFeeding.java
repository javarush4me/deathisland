package javarush.andreygoldyrev.deathisland.nature.behavior.lifecycle;

import javarush.andreygoldyrev.deathisland.nature.behavior.FeedService;
import javarush.andreygoldyrev.deathisland.nature.model.ENatureTypes;
import javarush.andreygoldyrev.deathisland.simulation.IslandSimulator;
import javarush.andreygoldyrev.deathisland.simulation.LocationSimulator;
import javarush.andreygoldyrev.deathisland.nature.model.Animal;
import javarush.andreygoldyrev.deathisland.nature.model.ENatureSpecies;
import javarush.andreygoldyrev.deathisland.nature.model.Nature;

import java.util.Set;


public class IslandFeeding implements ILifeCycle {
    private final FeedService feedService = new FeedService();

    @Override
    public void launch(IslandSimulator island) {
        island.getLocations().entrySet().parallelStream()
                .forEach(locationEntry -> {
                    LocationSimulator currentLocation = locationEntry.getValue();
                    for (ENatureSpecies species : currentLocation.getFloraAndFauna().keySet()) {
                        feedAnimalsInLocation(species, currentLocation);
                    }
                });
    }

    private void feedAnimalsInLocation(ENatureSpecies species, LocationSimulator currentLocation) {
        Set<Nature> natureOfThisSpeciesInLocation = currentLocation.getFloraAndFauna().get(species);
        for (Nature nature : natureOfThisSpeciesInLocation) {
            if (nature.getType().equals(ENatureTypes.ANIMAL) && !nature.isDead()) {
                Animal animal = (Animal) nature;
                feedService.feed(currentLocation, animal);
            }
        }
    }

    @Override
    public boolean applicable(ELifeCycleStages stage) {
        return stage.equals(ELifeCycleStages.FEED);
    }
}
