package javarush.andreygoldyrev.deathisland.nature.behavior;

import javarush.andreygoldyrev.deathisland.nature.behavior.moving.utils.*;
import javarush.andreygoldyrev.deathisland.simulation.LocationSimulator;
import javarush.andreygoldyrev.deathisland.nature.model.Animal;
import javarush.andreygoldyrev.deathisland.nature.model.Direction;
import javarush.andreygoldyrev.deathisland.nature.model.Nature;
import javarush.andreygoldyrev.deathisland.utils.Randomizer;

import java.util.*;

public class MoveService {
    private final List<IMovingCalculator> MOVING_CALCULATORS = Arrays.asList(new EastCalculator(), new WestCalculator(), new NorthCalculator(), new SouthCalculator());

    public void move(Animal animal, Map<Integer, LocationSimulator> locations, int currentLocationNumber) {
        Set<Nature> from = locations.get(currentLocationNumber).getFloraAndFauna().get(animal.getSpecies());

        int moveSpeed = 0;
        if (animal.getSpeed() > 0) {
            moveSpeed = Randomizer.createQuantity(animal.getSpeed());
        }

        int startLocationNumber = currentLocationNumber;
        int locationNumberToMoveTo = calculateNumberOfLocationToMoveTo(currentLocationNumber, moveSpeed, locations, animal);

        if (startLocationNumber != locationNumberToMoveTo) {
            Set<Nature> to = locations.get(locationNumberToMoveTo).getFloraAndFauna().get(animal.getSpecies());
            from.remove(animal);
            to.add(animal);
        }
    }

    private boolean isOvercrowded(Animal animal, LocationSimulator locationSimulatorToMoveTo) {
        return locationSimulatorToMoveTo.getFloraAndFauna().get(animal.getSpecies()).size() >= animal.getMaxQuantity();
    }

    private int calculateNumberOfLocationToMoveTo(int currentLocationNumber, int moveSpeed, Map<Integer, LocationSimulator> locations, Animal animal) {
        Direction direction = Randomizer.chooseDirection();
        Optional<IMovingCalculator> movingStrategy = MOVING_CALCULATORS.stream().filter(movingCalculator -> movingCalculator.applicable(direction)).findFirst();

        for (int i = 0; i < moveSpeed; i++) {
            int nextLocationNumber = movingStrategy.orElseThrow().calculateNumberOfLocationToMoveTo(currentLocationNumber);
            LocationSimulator nextLocation = locations.get(nextLocationNumber);
            if (nextLocationNumber > 0 && !isOvercrowded(animal, nextLocation)) {
                currentLocationNumber = nextLocationNumber;
            }
        }
        int locationNumberToMoveTo = currentLocationNumber;
        return locationNumberToMoveTo;
    }
}
