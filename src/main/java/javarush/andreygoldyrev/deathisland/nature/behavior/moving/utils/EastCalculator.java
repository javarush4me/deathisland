package javarush.andreygoldyrev.deathisland.nature.behavior.moving.utils;

import javarush.andreygoldyrev.deathisland.nature.model.Direction;
import javarush.andreygoldyrev.deathisland.utils.SimulationProperties;

public class EastCalculator implements IMovingCalculator {
    private SimulationProperties simulationProperties = SimulationProperties.getInstance();

    @Override
    public int calculateNumberOfLocationToMoveTo(int currentLocationNumber) {
        if (currentLocationNumber >= simulationProperties.getHeight() * simulationProperties.getWidth()) return 0;
        int tail = (currentLocationNumber) % simulationProperties.getWidth();
        if (tail > 0) return currentLocationNumber + 1;
        return 0;
    }

    @Override
    public boolean applicable(Direction direction) {
        return direction.equals(Direction.EAST);
    }
}
