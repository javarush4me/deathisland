package javarush.andreygoldyrev.deathisland.nature.behavior.lifecycle;

import javarush.andreygoldyrev.deathisland.nature.behavior.MoveService;
import javarush.andreygoldyrev.deathisland.nature.model.ENatureTypes;
import javarush.andreygoldyrev.deathisland.simulation.IslandSimulator;
import javarush.andreygoldyrev.deathisland.simulation.LocationSimulator;
import javarush.andreygoldyrev.deathisland.nature.model.Animal;
import javarush.andreygoldyrev.deathisland.nature.model.ENatureSpecies;
import javarush.andreygoldyrev.deathisland.nature.model.Nature;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public class IslandMigration implements ILifeCycle {
    private final MoveService moveService = new MoveService();

    @Override
    public void launch(IslandSimulator island) {
        for (Map.Entry<Integer, LocationSimulator> locationEntry : island.getLocations().entrySet()) {
            int currentLocationNumber = locationEntry.getKey();
            LocationSimulator currentLocation = locationEntry.getValue();
            for (ENatureSpecies species : currentLocation.getFloraAndFauna().keySet()) {
                moveNatureFromLocation(island.getLocations(), species, currentLocation, currentLocationNumber);
            }
        }
    }

    private void moveNatureFromLocation(Map<Integer, LocationSimulator> locations, ENatureSpecies species, LocationSimulator currentLocation, int currentLocationNumber) {
        Set<Nature> natureOfThisSpeciesInLocation = currentLocation.getFloraAndFauna().get(species);
        ArrayList<Nature> listCopy = new ArrayList<>(natureOfThisSpeciesInLocation);
        for (Nature nature : listCopy) {
            if (nature.getType().equals(ENatureTypes.ANIMAL) && !nature.isDead()) {
                Animal animal = (Animal) nature;
                moveService.move(animal, locations, currentLocationNumber);
            }
        }
    }

    @Override
    public boolean applicable(ELifeCycleStages stage) {
        return stage.equals(ELifeCycleStages.MOVE);
    }
}
