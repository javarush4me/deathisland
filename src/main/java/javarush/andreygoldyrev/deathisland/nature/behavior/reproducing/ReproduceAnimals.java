package javarush.andreygoldyrev.deathisland.nature.behavior.reproducing;

import javarush.andreygoldyrev.deathisland.nature.model.Animal;
import javarush.andreygoldyrev.deathisland.nature.model.ENatureTypes;
import javarush.andreygoldyrev.deathisland.nature.model.Nature;

import java.util.Set;


public class ReproduceAnimals implements IReproducingStrategy {
    @Override
    public void reproduce(Set<Nature> natureUnitOfThisTypeInLocation, Nature nature) {
        int offspringCount = 0;
        Animal animalToBeReproduced = (Animal) nature;

        for (Nature animal : natureUnitOfThisTypeInLocation) {
            if (!animal.isDead()) {
                offspringCount++;
            }
        }
        for (int i = 1; i <= offspringCount / 2; i++) {
            if (natureUnitOfThisTypeInLocation.size() < nature.getMaxQuantity()) {
                natureUnitOfThisTypeInLocation.add(animalToBeReproduced.clone());
            }
        }
    }

    @Override
    public boolean applicable(Nature nature) {
        return nature.getType().equals(ENatureTypes.ANIMAL);
    }
}
