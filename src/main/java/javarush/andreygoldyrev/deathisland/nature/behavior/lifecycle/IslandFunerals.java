package javarush.andreygoldyrev.deathisland.nature.behavior.lifecycle;

import javarush.andreygoldyrev.deathisland.nature.behavior.GraveyardService;
import javarush.andreygoldyrev.deathisland.simulation.IslandSimulator;
import javarush.andreygoldyrev.deathisland.simulation.LocationSimulator;

import java.util.Map;

public class IslandFunerals implements ILifeCycle {
    @Override
    public void launch(IslandSimulator island) {
        island.getGraveyardService().clearIsland(island.getLocations());
    }

    @Override
    public boolean applicable(ELifeCycleStages stage) {
        return stage.equals(ELifeCycleStages.BARY);
    }
}


