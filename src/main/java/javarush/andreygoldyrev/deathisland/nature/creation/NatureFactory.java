package javarush.andreygoldyrev.deathisland.nature.creation;

import com.fasterxml.jackson.databind.ObjectMapper;
import javarush.andreygoldyrev.deathisland.nature.model.ENatureSpecies;
import javarush.andreygoldyrev.deathisland.nature.model.Nature;
import lombok.SneakyThrows;

import java.nio.file.Files;
import java.nio.file.Path;


import static javarush.andreygoldyrev.deathisland.constants.Constants.NATURE_RESOURCES_PATH;

public class NatureFactory {
    private final ObjectMapper mapper = new ObjectMapper();


    @SneakyThrows
    public Nature createNatureUnit(ENatureSpecies species) {
        Path path = Path.of(NATURE_RESOURCES_PATH + species.name() + ".json");
        return mapper.readValue(Files.readAllBytes(path), species.getType().getClazz());
    }
}
