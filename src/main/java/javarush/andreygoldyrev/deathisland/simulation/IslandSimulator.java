package javarush.andreygoldyrev.deathisland.simulation;

import javarush.andreygoldyrev.deathisland.nature.behavior.GraveyardService;
import javarush.andreygoldyrev.deathisland.nature.behavior.lifecycle.*;
import lombok.Data;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


@Data
public class IslandSimulator {
    private Map<Integer, LocationSimulator> locations = new ConcurrentHashMap<>();
    private GraveyardService graveyardService = new GraveyardService();
    private final List<ILifeCycle> LIFE_CYCLE_STRATEGIES = Arrays.asList(new IslandSettlement(), new IslandMigration(), new IslandOrgy(), new IslandFeeding(), new IslandFunerals());

    public void simulateIslandLifeCycleStage(ELifeCycleStages stage) {
        Optional<ILifeCycle> lifeCycleStrategy = LIFE_CYCLE_STRATEGIES.stream().filter(strategy -> strategy.applicable(stage)).findFirst();
        lifeCycleStrategy.orElseThrow().launch(this);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry entry : locations.entrySet()) {
            builder.append(entry.getKey()).append(" ").append(entry.getValue()).append("\n");
        }
        return "ALIVE ANIMALS ON AN ISLAND" + "\n" + builder + "\n" + " #########################";
    }
}
