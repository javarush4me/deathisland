package javarush.andreygoldyrev.deathisland.simulation.strategies;

import javarush.andreygoldyrev.deathisland.nature.behavior.lifecycle.ELifeCycleStages;
import javarush.andreygoldyrev.deathisland.simulation.IslandSimulator;
import javarush.andreygoldyrev.deathisland.utils.SimulationProperties;
import javarush.andreygoldyrev.deathisland.utils.Statistics;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static javarush.andreygoldyrev.deathisland.constants.Constants.*;

public class SyncByDuration implements ISimulations {
    private SimulationProperties simulationProperties = SimulationProperties.getInstance();
    private AtomicInteger operationsCount = new AtomicInteger(0);

    @Override
    public void run() {
        IslandSimulator deathIslandSimulator = new IslandSimulator();
        System.out.println(SETTLING_THE_ISLAND);
        deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.SETTLE);
        Statistics.printStatistics(deathIslandSimulator);

        try (ScheduledExecutorService executor = Executors.newScheduledThreadPool(1)) {

            executor.scheduleAtFixedRate(() -> {
                System.out.println(STARTING_TO_MOVE);
                deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.MOVE);
                operationsCount.incrementAndGet();
                System.out.println(STARTING_TO_REPRODUCE);
                deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.REPRODUCE);
                operationsCount.incrementAndGet();
                System.out.println(STARTING_TO_FEED);
                deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.FEED);
                operationsCount.incrementAndGet();
                System.out.println(STARTING_TO_BARY);
                deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.BARY);
                operationsCount.incrementAndGet();
                Statistics.printStatistics(deathIslandSimulator);
                operationsCount.incrementAndGet();
            }, simulationProperties.getFixedDelay(), simulationProperties.getFixedPeriod(), TimeUnit.MICROSECONDS);
            try {
                TimeUnit.SECONDS.sleep(simulationProperties.getSimulationDuration());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            executor.shutdownNow();
        }
        System.out.println(OPERATIONS_FINISHED + operationsCount);
    }

    @Override
    public boolean applicable(ESimulations type) {
        return type.equals(ESimulations.SYNC_BY_DURATION);
    }
}
