package javarush.andreygoldyrev.deathisland.simulation.strategies;

public interface ISimulations {
    void run();

    boolean applicable(ESimulations type);
}
