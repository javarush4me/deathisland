package javarush.andreygoldyrev.deathisland.simulation.strategies;

import javarush.andreygoldyrev.deathisland.nature.behavior.lifecycle.ELifeCycleStages;
import javarush.andreygoldyrev.deathisland.simulation.IslandSimulator;
import javarush.andreygoldyrev.deathisland.utils.SimulationProperties;
import javarush.andreygoldyrev.deathisland.utils.Statistics;

import java.util.concurrent.CompletableFuture;

import static javarush.andreygoldyrev.deathisland.constants.Constants.*;


public class AsyncByMoves implements ISimulations {
    private SimulationProperties simulationProperties = SimulationProperties.getInstance();

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();

        IslandSimulator deathIslandSimulator = new IslandSimulator();
        System.out.println(SETTLING_THE_ISLAND);
        deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.SETTLE);
        Statistics.printStatistics(deathIslandSimulator);

        for (int i = 1; i <= simulationProperties.getSimulationMoves(); i++) {
            System.out.println(MOVE_NUMBER + i);
            CompletableFuture<Void> move = CompletableFuture.runAsync(() -> {
                System.out.println(STARTING_TO_MOVE);
                deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.MOVE);
            });

            CompletableFuture<Void> reproduce = CompletableFuture.runAsync(() -> {
                System.out.println(STARTING_TO_REPRODUCE);
                deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.REPRODUCE);

            });

            CompletableFuture<Void> feed = CompletableFuture.runAsync(() -> {
                System.out.println(STARTING_TO_FEED);
                deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.FEED);


            });
            CompletableFuture<Void> bary = feed.thenRunAsync(() -> {
                System.out.println(STARTING_TO_BARY);
                deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.BARY);


            });
            CompletableFuture.allOf(move, reproduce, feed, bary).join();
            Statistics.printStatistics(deathIslandSimulator);

        }
        long endTime = System.currentTimeMillis();
        long elapsedTime = endTime - startTime;
        System.out.printf("ASYNC took = %d milliseconds", elapsedTime);

    }

    @Override
    public boolean applicable(ESimulations typeOfSimulation) {
        return typeOfSimulation.equals(ESimulations.ASYNC_BY_MOVES);
    }
}
