package javarush.andreygoldyrev.deathisland.simulation.strategies;

public enum ESimulations {
    ASYNC_BY_MOVES,
    SYNC_BY_MOVES,
    ASYNC_BY_DURATION,
    SYNC_BY_DURATION
}
