package javarush.andreygoldyrev.deathisland.simulation.strategies;

import javarush.andreygoldyrev.deathisland.nature.behavior.lifecycle.ELifeCycleStages;
import javarush.andreygoldyrev.deathisland.simulation.IslandSimulator;
import javarush.andreygoldyrev.deathisland.utils.SimulationProperties;
import javarush.andreygoldyrev.deathisland.utils.Statistics;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static javarush.andreygoldyrev.deathisland.constants.Constants.*;


public class AsyncByDuration implements ISimulations {
    private SimulationProperties simulationProperties = SimulationProperties.getInstance();
    private AtomicInteger operationsCount = new AtomicInteger(0);
    private IslandSimulator deathIslandSimulator = new IslandSimulator();

    @Override
    public void run() {

        System.out.println(SETTLING_THE_ISLAND);
        deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.SETTLE);
        Statistics.printStatistics(deathIslandSimulator);

        try (ScheduledExecutorService executor = Executors.newScheduledThreadPool(5)) {

            executor.scheduleAtFixedRate(() -> {
                System.out.println(STARTING_TO_MOVE);
                deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.MOVE);
                operationsCount.incrementAndGet();
            }, simulationProperties.getMigrationDelay(), simulationProperties.getMigrationPeriod(), TimeUnit.MICROSECONDS);

            executor.scheduleAtFixedRate(() -> {
                System.out.println(STARTING_TO_REPRODUCE);
                deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.REPRODUCE);
                operationsCount.incrementAndGet();
            }, simulationProperties.getReproductionDelay(), simulationProperties.getReproductionPeriod(), TimeUnit.MICROSECONDS);

            executor.scheduleAtFixedRate(() -> {
                System.out.println(STARTING_TO_FEED);
                deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.FEED);
                operationsCount.incrementAndGet();
            }, simulationProperties.getFeedingDelay(), simulationProperties.getFeedingPeriod(), TimeUnit.MICROSECONDS);

            executor.scheduleAtFixedRate(() -> {
                Statistics.printStatistics(deathIslandSimulator);
                operationsCount.incrementAndGet();
            }, simulationProperties.getStatisticsDelay(), simulationProperties.getStatisticsPeriod(), TimeUnit.MICROSECONDS);

            executor.scheduleAtFixedRate(() -> {
                System.out.println(STARTING_TO_BARY);
                deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.BARY);
                operationsCount.incrementAndGet();
            }, simulationProperties.getBuryingDelay(), simulationProperties.getBuryingPeriod(), TimeUnit.MICROSECONDS);

            try {
                TimeUnit.SECONDS.sleep(simulationProperties.getSimulationDuration());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            executor.shutdownNow();
        }
        System.out.println(OPERATIONS_FINISHED + operationsCount);
    }

    @Override
    public boolean applicable(ESimulations type) {
        return type.equals(ESimulations.ASYNC_BY_DURATION);
    }
}
