package javarush.andreygoldyrev.deathisland.simulation.strategies;

import javarush.andreygoldyrev.deathisland.nature.behavior.lifecycle.ELifeCycleStages;
import javarush.andreygoldyrev.deathisland.simulation.IslandSimulator;
import javarush.andreygoldyrev.deathisland.utils.SimulationProperties;
import javarush.andreygoldyrev.deathisland.utils.Statistics;

import static javarush.andreygoldyrev.deathisland.constants.Constants.*;


public class SyncByMoves implements ISimulations {
    private SimulationProperties simulationProperties = SimulationProperties.getInstance();

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();

        IslandSimulator deathIslandSimulator = new IslandSimulator();
        System.out.println("Settling the island");
        deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.SETTLE);
        Statistics.printStatistics(deathIslandSimulator);

        for (int i = 1; i < simulationProperties.getSimulationMoves(); i++) {
            System.out.println(MOVE_NUMBER + i);
            System.out.println(STARTING_TO_MOVE);
            deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.MOVE);
            System.out.println(STARTING_TO_REPRODUCE);
            deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.REPRODUCE);
            System.out.println(STARTING_TO_FEED);
            deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.FEED);
            System.out.println(STARTING_TO_BARY);
            deathIslandSimulator.simulateIslandLifeCycleStage(ELifeCycleStages.BARY);
            Statistics.printStatistics(deathIslandSimulator);
        }

        long endTime = System.currentTimeMillis();
        long elapsedTime = endTime - startTime;
        System.out.printf("SYNC took = %d milliseconds", elapsedTime);
    }

    @Override
    public boolean applicable(ESimulations type) {
        return type.equals(ESimulations.SYNC_BY_MOVES);
    }
}
