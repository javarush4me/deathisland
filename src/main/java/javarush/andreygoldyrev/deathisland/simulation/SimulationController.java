package javarush.andreygoldyrev.deathisland.simulation;

import javarush.andreygoldyrev.deathisland.simulation.strategies.*;

import java.util.List;
import java.util.Optional;

public class SimulationController {
    private static final List<ISimulations> SIMULATION_STRATEGIES = List.of(new AsyncByMoves(), new SyncByMoves(), new AsyncByDuration(), new SyncByDuration());

    public static void startSimulation(ESimulations simulationType) {
        Optional<ISimulations> simulation = SIMULATION_STRATEGIES.stream().filter(strategy -> strategy.applicable(simulationType)).findFirst();
        simulation.orElseThrow().run();
    }
}
