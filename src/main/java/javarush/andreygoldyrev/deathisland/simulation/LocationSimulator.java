package javarush.andreygoldyrev.deathisland.simulation;

import javarush.andreygoldyrev.deathisland.nature.model.ENatureTypes;
import javarush.andreygoldyrev.deathisland.nature.model.Nature;
import javarush.andreygoldyrev.deathisland.nature.creation.NatureFactory;
import javarush.andreygoldyrev.deathisland.nature.model.ENatureSpecies;
import javarush.andreygoldyrev.deathisland.utils.Randomizer;
import lombok.Data;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;


@Data
public class LocationSimulator {
    private Map<ENatureSpecies, Set<Nature>> floraAndFauna = new ConcurrentHashMap<>();
    private NatureFactory natureFactory = new NatureFactory();

    public void settleLocation() {
        CompletableFuture<Void> settleAnimals = CompletableFuture.runAsync(() -> {
            naturalize(ENatureTypes.ANIMAL);
        });
        CompletableFuture<Void> settlePlants = CompletableFuture.runAsync(() -> {
            naturalize(ENatureTypes.PLANT);
        });
        CompletableFuture.allOf(settleAnimals, settlePlants).join();
    }

    private void naturalize(ENatureTypes natureType) {
        Arrays.stream(ENatureSpecies.values())
                .filter(species -> species.getType().equals(natureType))
                .parallel()
                .forEach(
                        species -> {
                            Set<Nature> natureSet = ConcurrentHashMap.newKeySet();
                            Nature natureUnitConfig = natureFactory.createNatureUnit(species);
                            if (natureType.equals(ENatureTypes.PLANT)) {
                                for (int i = 0; i < natureUnitConfig.getMaxQuantity(); i++) {
                                    natureSet.add(natureFactory.createNatureUnit(species));
                                }
                                floraAndFauna.put(species, natureSet);
                            } else {
                                for (int i = 0; i < Randomizer.createQuantity(natureUnitConfig.getMaxQuantity()); i++) {
                                    natureSet.add(natureFactory.createNatureUnit(species));
                                }
                                floraAndFauna.put(species, natureSet);
                            }
                        }
                );
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Location's floraAndFauna: " + "\r\n ");
        for (Map.Entry<ENatureSpecies, Set<Nature>> entry : floraAndFauna.entrySet()) {
            stringBuilder.append(" " + entry.getKey().name() + " " + entry.getValue().size() + "\r\n ");
        }
        stringBuilder.append("-------------------------");
        return stringBuilder.toString();
    }
}
