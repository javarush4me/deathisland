package javarush.andreygoldyrev.deathisland.utils;

import javarush.andreygoldyrev.deathisland.nature.model.Direction;

import java.util.Random;


public class Randomizer {
    public static int createQuantity(int max) {
        return new Random().nextInt(max);
    }

    public static int chanceToCatchPrey() {
        return new Random().nextInt(100);
    }

    public static Direction chooseDirection() {
        int amount = Direction.values().length;
        int number = new Random().nextInt(amount);
        return Direction.values()[number];
    }
}

