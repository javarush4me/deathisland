package javarush.andreygoldyrev.deathisland.utils;


import lombok.Getter;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;

@Getter

public class SimulationProperties {
    private static SimulationProperties instance;
    private FileBasedConfiguration configuration;

    private SimulationProperties() {
        Parameters params = new Parameters();
        FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
                new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
                        .configure(params.properties()
                                .setFileName("simulation.properties"));
        try {
            configuration = builder.getConfiguration();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static synchronized SimulationProperties getInstance() {
        if (instance == null) {
            instance = new SimulationProperties();
        }
        return instance;
    }

    public int getWidth() {
        return configuration.getInt("map.width");
    }

    public int getHeight() {
        return configuration.getInt("map.height");
    }

    public int getSimulationMoves() {
        return configuration.getInt("simulation.moves");
    }

    public int getSimulationDuration() {
        return configuration.getInt("simulation.duration");
    }

    public long getMigrationDelay() {
        return configuration.getLong("move.delay");
    }

    public long getMigrationPeriod() {
        return configuration.getLong("move.period");
    }

    public long getReproductionDelay() {
        return configuration.getLong("reproduce.delay");
    }

    public long getReproductionPeriod() {
        return configuration.getLong("reproduce.period");
    }

    public long getFeedingDelay() {
        return configuration.getLong("feed.delay");
    }

    public long getFeedingPeriod() {
        return configuration.getLong("feed.period");
    }

    public long getBuryingDelay() {
        return configuration.getLong("barry.delay");
    }

    public long getBuryingPeriod() {
        return configuration.getLong("barry.period");
    }

    public long getStatisticsDelay() {
        return configuration.getLong("stat.delay");
    }

    public long getStatisticsPeriod() {
        return configuration.getLong("stat.period");
    }

    public long getFixedDelay() {
        return configuration.getLong("fixed.delay");
    }

    public long getFixedPeriod() {
        return configuration.getLong("fixed.period");
    }
}
