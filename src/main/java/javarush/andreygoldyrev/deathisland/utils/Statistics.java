package javarush.andreygoldyrev.deathisland.utils;

import javarush.andreygoldyrev.deathisland.simulation.IslandSimulator;
import lombok.SneakyThrows;

public class Statistics {

    @SneakyThrows
    public static void printStatistics(IslandSimulator island) {
        System.out.println(island.toString());
        System.out.println(island.getGraveyardService().toString());
    }
}
